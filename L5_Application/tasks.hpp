/*
 *     SocialLedge.com - Copyright (C) 2013
 *
 *     This file is part of free software framework for embedded processors.
 *     You can use it and/or distribute it as long as this copyright header
 *     remains unmodified.  The code is free for personal use and requires
 *     permission to use in a commercial product.
 *
 *      THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 *      OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 *      MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 *      I SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 *      CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *     You can reach the author of this software at :
 *          p r e e t . w i k i @ g m a i l . c o m
 */

/**
 * @file
 * @brief Contains FreeRTOS Tasks
 */
#ifndef TASKS_HPP_
#define TASKS_HPP_

#include "scheduler_task.hpp"
#include "soft_timer.hpp"
#include "command_handler.hpp"
#include "wireless.h"
#include "char_dev.hpp"

#include "FreeRTOS.h"
#include "semphr.h"

#include "stdio.h"
#include "stdint.h"



/**
 * Terminal task is our UART0 terminal that handles our commands into the board.
 * This also saves and restores the "disk" telemetry.  Disk telemetry variables
 * are automatically saved and restored across power-cycles to help us preserve
 * any non-volatile information.
 */
class terminalTask : public scheduler_task
{
    public:
        terminalTask(uint8_t priority);     ///< Constructor
        bool regTlm(void);                  ///< Registers telemetry
        bool taskEntry(void);               ///< Registers commands.
        bool run(void *p);                  ///< The main loop

    private:
        // Command channels device and input command str
        typedef struct {
            CharDev *iodev; ///< The IO channel
            str *cmdstr;    ///< The command string
            bool echo;      ///< If input should be echo'd back
        } cmdChan_t;

        VECTOR<cmdChan_t> mCmdIface;   ///< Command interfaces
        CommandProcessor mCmdProc;     ///< Command processor
        uint16_t mCommandCount;        ///< terminal command count
        uint16_t mDiskTlmSize;         ///< Size of disk variables in bytes
        char *mpBinaryDiskTlm;         ///< Binary disk telemetry
        SoftTimer mCmdTimer;           ///< Command timer

        cmdChan_t getCommand(void);
        void addCommandChannel(CharDev *channel, bool echo);
        void handleEchoAndBackspace(cmdChan_t *io, char c);
        bool saveDiskTlm(void);
};

/**
 * Remote task is the task that monitors the IR remote control signals.
 * It can "learn" remote control codes by typing "learn" into the UART0 terminal.
 * Thereafter, if a user enters a 2-digit number through a remote control, then
 * your function handleUserEntry() is called where you can take an action.
 */
class remoteTask : public scheduler_task
{
    public:
        remoteTask(uint8_t priority);   ///< Constructor
        bool init(void);                ///< Inits the task
        bool regTlm(void);              ///< Registers non-volatile variables
        bool taskEntry(void);           ///< One time entry function
        bool run(void *p);              ///< The main loop

    private:
        /** This function is called when a 2-digit number is decoded */
        void handleUserEntry(int num);

        /**
         * @param code  The IR code
         * @param num   The matched number 0-9 that mapped the IR code.
         * @returns true if the code has been successfully mapped to the num
         */
        bool getNumberFromCode(uint32_t code, uint32_t& num);

        uint32_t mNumCodes[10];      ///< IR Number codes
        uint32_t mIrNumber;          ///< Current IR number we're decoding
        SemaphoreHandle_t mLearnSem; ///< Semaphore to enable IR code learning
        SoftTimer mIrNumTimer;       ///< Time-out for user entry for 1st and 2nd digit
};

/**
 * Nordic wireless task to participate in the mesh network and handle retry logic
 * such that packets are resent if an ACK has not been received
 */
class wirelessTask : public scheduler_task
{
    public:
        wirelessTask(uint8_t priority) :
            scheduler_task("wireless", 512, priority)
        {
            /* Nothing to init */
        }

        bool run(void *p)
        {
            wireless_service(); ///< This is a non-polling function if FreeRTOS is running.
            return true;
        }
};

/**
 * GPIO task
 * This task ties an onboard switch to an onboard LED
 * and an external switch to an external LED
 */
class gpio_lab : public scheduler_task
{
    public:
        gpio_lab():scheduler_task("gpio", 2000, PRIORITY_LOW)
        {
            // Nothing
        }

        bool init(void){
            /**
             * Initialize function which runs once before run function
             * Using P1.22 for LED and P1.28 for switch
             */
            // Using external switch and LED
            // Make P1.22 output
            LPC_GPIO1 -> FIODIR |= (1 << 22);
            // Make P1.28 input
            LPC_GPIO1 -> FIODIR &= ~(1 << 28);
            // Using internal switch and LED
            // Make P1.0 output
            LPC_GPIO1 -> FIODIR |= (1 << 0);
            // Make P1.9 input
            LPC_GPIO1 -> FIODIR &= ~(1 << 9);
            return true;
        }

        bool run(void *p){
            /**
             * Tie external output LED to switch
             */
            if(LPC_GPIO1 -> FIOPIN & (1 << 28)){
                // When input is high, make output low, turning on LED
                LPC_GPIO1 -> FIOCLR = (1 << 22);
            }else{
                // When input is low, make output high, turning off LED
                LPC_GPIO1 -> FIOSET = (1 << 22);
            }
            /**
             * Tie internal output LED to switch
             */
            if(LPC_GPIO1 -> FIOPIN & (1 << 9)){
                // When input is high, make output low, turning on LED
                LPC_GPIO1 -> FIOCLR = (1 << 0);
            }else{
                // When input is low, make output high, turning off LED
                LPC_GPIO1 -> FIOSET = (1 << 0);
            }
            return true;
        }
};

/**
 * SPI task
 * This task tests the SPI bus using the
 * AT45 flash onboard
 */
class spi_lab : public scheduler_task
{
    public:
        spi_lab():scheduler_task("spi", 2000, PRIORITY_LOW)
        {
            // Nothing
        }

        bool init(void){
            LPC_SC -> PCONP |= (1 << 10);     // SPI1 Power Enable
            LPC_SC -> PCLKSEL0 &= ~(3 << 20); // Clear clock Bits
            LPC_SC -> PCLKSEL0 |=  (1 << 20); // CLK / 1

            // Select MISO, MOSI, and SCK pin-select functionality
            LPC_PINCON -> PINSEL0 &= ~( (3 << 14) | (3 << 16) | (3 << 18) );
            LPC_PINCON -> PINSEL0 |=  ( (2 << 14) | (2 << 16) | (2 << 18) );

            LPC_SSP1 -> CR0 = 7;          // 8-bit mode
            LPC_SSP1 -> CR1 = (1 << 1);   // Enable SSP as Master
            LPC_SSP1 -> CPSR = 8;         // SCK speed = CPU / 8

            // Setting chip select (P0.6) to low to enable flash chip
            LPC_GPIO0 -> FIODIR |= (1 << 6);
            LPC_GPIO0 -> FIOCLR = (1 << 6);
            return true;
        }

        // Function which exchanges bytes
        uint8_t exchange_byte(uint8_t out){
            LPC_SSP1 -> DR = out;
            while(LPC_SSP1 -> SR & (1 << 4)); // Keep waiting until not busy anymore
            return LPC_SSP1 -> DR;
        }

        bool run(void *p){
            // Enable flash chip
            LPC_GPIO0 -> FIOCLR = (1 << 6);

            // Send opcode
            char c = 0x9F;
            printf("ManID Opcode:    %X\n", c);
            exchange_byte(c);

            // Print Manufacturer ID
            printf("Manufacturer ID: %X\n", exchange_byte(0xFF));

            // Print Device ID byte 1
            printf("Device ID_1:     %X\n", exchange_byte(0xFF));

            // Print Device ID byte 2
            printf("Device ID_2:     %X\n", exchange_byte(0xFF));

            // Print Device ID byte 2
            printf("EDI:             %X\n", exchange_byte(0xFF));

            // Print Device ID byte 2
            printf("EDI:             %X\n\n", exchange_byte(0xFF));

            // Disable flash chip
            LPC_GPIO0 -> FIOSET = (1 << 6);

            vTaskDelay(20);

            // Enable flash chip
            LPC_GPIO0 -> FIOCLR = (1 << 6);

            // Sending D7 to retrieve status register
            c = 0xD7;
            exchange_byte(c);
            printf("Status Reg Op:   %X\n", c);

            // Reading 2 bytes of status register
            char c1 = exchange_byte(0xFF);
            char c2 = exchange_byte(0xFF);

            printf("Status Register: %X%X\n\n", c1, c2);

            // Printing both status register bytes and accompanying information
            printf("Byte 1\n------\n");

            int tmp = ((c1 >> 7) & 1) ? 1 : 0;
            printf("Ready/Busy status:      %i", tmp);
            printf("\n0: Device is busy\n1: Device is ready\n");

            tmp = ((c1 >> 6) & 1) ? 1 : 0;
            printf("Compare Result:         %i", tmp);
            printf("\n0: Main memory matches buffer data\n"
                    "1: Main memory does not match buffer data\n");

            printf("Density Code:           ");
            for(int i = 5; i > 1; i--){
                tmp = ((c1 >> i) & 1) ? 1 : 0;
                printf("%i", tmp);
            }
            printf("\n");

            tmp = ((c1 >> 1) & 1) ? 1 : 0;
            printf("Sector Protection:      %i", tmp);
            printf("\n0: Sector protection is disabled\n1: Sector protection is enabled\n");

            tmp = ((c1 >> 0) & 1) ? 1 : 0;
            printf("Page Size Config:       %i", tmp);
            printf("\n0: Device is configured for standard DataFlash page size (528 bytes)\n"
                    "1: Device is configured for \"power of two\" binary page size (512 bytes)\n");

            printf("\nByte 2\n------\n");

            tmp = ((c2 >> 7) & 1) ? 1 : 0;
            printf("Ready/Busy status:      %i", tmp);
            printf("\n0: Device is busy\n1: Device is ready\n");

            tmp = ((c2 >> 6) & 1) ? 1 : 0;
            printf("Reserved for future:    %i", tmp);
            printf("\nReserved for future use\n");

            tmp = ((c2 >> 5) & 1) ? 1 : 0;
            printf("Erase/Program Error:    %i", tmp);
            printf("\n0: Erase or program operation was successful\n"
                    "1: Erase or program error detected\n");

            tmp = ((c2 >> 4) & 1) ? 1 : 0;
            printf("Reserved for future:    %i", tmp);
            printf("\nReserved for future use\n");

            tmp = ((c2 >> 3) & 1) ? 1 : 0;
            printf("Sector Lockdown EN:     %i", tmp);
            printf("\n0: Sector Lockdown command is disabled\n"
                    "1: Sector Lockdown command is enabled\n");

            tmp = ((c2 >> 2) & 1) ? 1 : 0;
            printf("Program Suspend Status: %i", tmp);
            printf("\n0: No program operation has been suspended while using Buffer 2\n"
                    "1: A sector is program suspended while using Buffer 2\n");

            tmp = ((c2 >> 1) & 1) ? 1 : 0;
            printf("Program Suspend Status: %i", tmp);
            printf("\n0: No program operation has been suspended while using Buffer 1\n"
                    "1: A sector is program suspended while using Buffer 1\n");

            tmp = ((c2 >> 0) & 1) ? 1 : 0;
            printf("Erase Suspend:          %i", tmp);
            printf("\n0: No sectors are erase suspended\n1: A sector is erase suspended\n\n");

            // Disable flash chip
            LPC_GPIO0 -> FIOSET = (1 << 6);

            vTaskDelay(10000);

            return true;
        }
};


/**
 * Periodic callback dispatcher task
 * This task gives the semaphores that end up calling functions at periodic_callbacks.cpp
 */
class periodicSchedulerTask : public scheduler_task
{
    public:
        periodicSchedulerTask(bool kHz=0);
        bool init(void);
        bool regTlm(void);
        bool run(void *p);

    private:
        bool handlePeriodicSemaphore(const uint8_t index, const uint8_t frequency);
        const uint8_t mKHz; // Periodic dispatcher should use 1Khz callback too
};

#endif /* TASKS_HPP_ */
